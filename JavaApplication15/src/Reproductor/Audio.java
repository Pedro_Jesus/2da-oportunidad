/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Reproductor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.Control;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JSlider;

/**
 *
 * @author Pedro
 */
public class Audio implements Runnable {

    public Audio(String r, Media C) {
        Controlador = C;
        ruta = r;
        try {
            audioFile = AudioSystem.getAudioInputStream(new File(this.ruta).getAbsoluteFile());
            clip = AudioSystem.getClip();
            clip.open(audioFile);
            Controlador.jLabel4.setText(tiempo(clip.getMicrosecondLength()));
        } catch (FileNotFoundException ex) {

        } catch (LineUnavailableException | UnsupportedAudioFileException | IOException ex) {
            Logger.getLogger(Audio.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void run() {
        int t;
        time = 0L;
        while (true) {
            t = (int) clip.getMicrosecondPosition() / 1000000;

            if (estado == MOD_TIEMPO) {
                o = true;
                Controlador.seleccion(0);
                clip.stop();
                estado = ESPERA;
            }
            if (estado == PAUSE) {
                time = clip.getMicrosecondPosition();
                clip.stop();
                Controlador.seleccion(PAUSE);
                estado = ESPERA;
            }
            if (estado == PLAY) {
                if (o) {
                    time = (long) Controlador.jSlider1.getValue() * 1000000;
                } else {
                    o = false;
                }

                clip.setMicrosecondPosition(time);
                clip.start();
                time = 0L;
                Controlador.seleccion(PLAY);
                estado = ESPERA;
            }
            if (estado == STOP) {
                clip.stop();
                time = 0L;
                Controlador.seleccion(STOP);
                o = true;
                Controlador.jSlider1.setValue(0);
                estado = ESPERA;
            }
            //tiempo(clip.getMicrosecondPosition());
            Controlador.jLabel5.setText(tiempo(clip.getMicrosecondPosition()));
            if (!o) {
                slider(t, (int) clip.getMicrosecondLength() / 1000000);
            }
            /*tm=(int)clip.getMicrosecondPosition()/1000000;
            Controlador.jLabel5.setText(tm+"");*/

        }
    }
//getMicrosecondPosition()-ubicacion del tiempo
//getMicrosecondlenth()-longitud

    public void Accion(int i) {
        estado = i;
    }

    public String tiempo(long l) {
        int i = (int) l / 1000000;
        int e = i / 60;
        i-=e*60;
        String t = "";
        if (e < 10) {
            if (i < 10) {
                t += "0" + e + ":0" + i;//Controlador.jLabel5.setText("0"+e+":0"+i);
            } else {
                t += "0" + e + ":" + i;//Controlador.jLabel5.setText("0"+e+":"+i);
            }
        } else {
            if (i < 10) {
                t += e + ":0" + i;//Controlador.jLabel5.setText(e+":0"+i);
            } else {
                t += e + ":" + i;//Controlador.jLabel5.setText(e+":"+i);
            }
        }
        return t;
    }

    public void slider(int p, int l) {
        Controlador.jSlider1.setMaximum(l);
        Controlador.jSlider1.setValue(p);
    }

    private boolean o = false;
    private int estado = 4, tm;
    private Long time;
    private final int MOD_TIEMPO = 0,
            PAUSE = 1,
            PLAY = 2,
            STOP = 3,
            ESPERA = 4;
    private Media Controlador;
    private String ruta;
    private Clip clip;
    private AudioInputStream audioFile;
}
